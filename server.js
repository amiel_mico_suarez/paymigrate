(async function(){'use strict';
	require('dotenv').config();
	
	let DB = {};
	let MODEL = {};
	const mongoose = require('mongoose');
	const moment = require('moment');
	const chalk = require('chalk');
	mongoose.Promise = global.Promise;

	const connectToDb = (key)=>{
		DB = {};
		MODEL = {};
		
		let connection = mongoose.createConnection("mongodb://"+process.env.DB_USERNAME+":"+process.env.DB_PASSWORD+"@"+process.env.DB_HOST+":"+process.env.DB_PORT+"/"+process.env[key]);
		return new Promise((resolve) => {
			setTimeout(()=>{resolve(connection)},500)
		});
	}

	const disconnectDb = (connection)=>{
		connection.close();
		return new Promise((resolve)=>{
			setTimeout(()=>{resolve()},500)
		})
	}

	const dropCollection = (key, connection)=>{
		connection.db.dropCollection(key)
		return new Promise((resolve)=>{
			setTimeout(()=>{resolve()},200)
		})
	}

	const queryAll = (name)=>{
		return new Promise((resolve) => {
			DB[name].find({}, (err, res)=>{
				resolve(res)
			})
		})
	};

	const populateDb = (connection)=>{
		Object.keys(MODEL).forEach((key)=>{
			if (DB[key] == undefined){DB[key] = connection.model(key, MODEL[key])}
		});
	}

	const saveDatabaseEntry = (entry)=>{
		return new Promise((resolve, reject)=>{
			entry.save((err)=>{
				resolve(entry)
			})
		})
	}

	const addZeroToNumber = (num)=>{
		if (num < 10){
			return "0" + String(num)
		} else {
			return String(num)
		}
	}

	const convertTrashDateFormat = (date_obj)=>{
		return date_obj.year + "-" + addZeroToNumber(date_obj.month) + "-" + addZeroToNumber(date_obj.day)
	}

	const consoleBreak = ()=>{
		console.log(chalk.black("..."));
		console.log(chalk.black("..."));
		return new Promise((resolve)=>{
			setTimeout(()=>{resolve()},400)
		})
	}

	console.log(chalk.cyan("CONNECTING TO OLD DATABASE"));
	let old_db = await connectToDb("DB_NAME_OLD");
	MODEL["schemas"] = require('./models_old/schemas.js')(mongoose);
	MODEL["settings"] = require('./models_old/settings.js')(mongoose);
	await consoleBreak();

	console.log(chalk.yellow("CONSTRUCTING DB MODELS"));
	populateDb(old_db);
	let other_collections = await queryAll("schemas");
	other_collections.forEach((item)=>{MODEL[item.key] = new mongoose.Schema({_prop: Object})});
	populateDb(old_db);

	console.log(chalk.magenta("FETCHING CONTENTS OF OLD DATABASE"));
	let old_logs = await queryAll("logs");
	let old_users = await queryAll("users");
	await consoleBreak();

	console.log(chalk.yellow("DISCONNECTING OLD DATABASE"));
	await disconnectDb(old_db);
	await consoleBreak();

	console.log(chalk.cyan("CONNECTING TO NEW DATABASE"));
	let new_db = await connectToDb("DB_NAME_NEW");

	console.log(chalk.yellow("RESETING NEW DATABASE MODELS"));
	await dropCollection("users", new_db);
	await dropCollection("payslips", new_db);
	DB["users"] = new_db.model('users', require('./models_new/users.js'));
	DB["payslips"] = new_db.model('payslips', require('./models_new/payslips.js'));
	await consoleBreak();

	console.log(chalk.magenta("MIGRATING USER DATA"));
	let new_users = {};
	let user_index = 0;
	for (const user of old_users) {
		let profile = {...user._prop.slack.profile};
		let new_user = await saveDatabaseEntry(new DB["users"]({
			"slack_id" : user._prop.slack.id,
			"email" : profile.email,
			"real_name" : profile.real_name,
			"name" : profile.display_name,
			"payslips" : [],
			"permissions" : [],
			"convo" : {
				"actions" : [],
				"params" : {},
				"last_message" : ""
			},
			"password" : user._prop.payslip_password
		}))

		new_users[profile.email] = new_user;
		user_index++;
		console.log(chalk.magenta(user_index + "/" + old_users.length))
	};
	console.log(chalk.magenta("USER DATA SUCCESSFULLY MIGRATED"));
	await consoleBreak();

	console.log(chalk.magenta("MIGRATING PAYSLIP DATA"));
	let payslip_index = 0;
	let payslip_total = 0;

	for (const log of old_logs) {for (const payslip_data of log._prop.data) {payslip_total++}}
	for (const log of old_logs) {
		for (const payslip_data of log._prop.data) {
			let foreign_user = new_users[payslip_data.employeeemail];

			if (foreign_user){
				let new_payslip = await saveDatabaseEntry(new DB["payslips"]({
					"user" : String(foreign_user._id),
					"date_start" : moment(convertTrashDateFormat(log._prop.cutoff_date_start), "YYYY-MM-DD").toDate(),
					"date_end" : moment(convertTrashDateFormat(log._prop.cutoff_date_end), "YYYY-MM-DD").toDate(),
					"data" : {...payslip_data},
					"fetch_count" : 0,
					"notified" : true,
					"published" : true,
				}));
	
				foreign_user.payslips.push(String(new_payslip._id));
				foreign_user.markModified("payslips");
	
				delete new_users[payslip_data.employeeemail];
				new_users[payslip_data.employeeemail] = await saveDatabaseEntry(foreign_user);
	
				payslip_index++;
				console.log(chalk.magenta(payslip_index + "/" + payslip_total));
			}
		}
	}

	let orphaned_payslips = payslip_total - payslip_index
	if (orphaned_payslips > 0){
		console.log(chalk.red(payslip_index + " PAYSLIP DATA WAS(WERE) MIRGATED"));
		console.log(chalk.red(orphaned_payslips + " ORPHANED PAYSLIPS WAS(WERE) NOT MIGRATED"));
	} else {
		console.log(chalk.magenta("ALL PAYSLIP DATA SUCCESSFULLY MIGRATED"));
	}
	await consoleBreak();
	console.log("MIGRATION SUCCESSFUL");
	
}());