module.exports = function(mongoose) {
	var Schema = mongoose.Schema;

	var model_schema = new Schema({
		key:String,
		parameters:Object
	});
	
	return model_schema;
}