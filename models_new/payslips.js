(function(){'use strict';
	const mongoose = require('mongoose');
	const schema = mongoose.Schema;

	const PayslipSchema = new schema({
		date_start: {
			type: Date,
			required: true,
		},

		date_end: {
			type: Date,
			required: true,
		},

		user: {
			type: String,
			required: true,
			ref: 'users'
		},

		published: {
			type: Boolean,
			default: false,
		},

		notified: {
			type: Boolean,
			default: false,
		},

		data: {
			type: Object,
			required: true,
		},

		fetch_count: {
			type: Number,
			default: 0,
		},
	});


	module.exports = PayslipSchema;
}())