(function(){'use strict';
	const mongoose = require('mongoose');
	const schema = mongoose.Schema;

	const UserSchema = new schema({
		name: { 
			type: String,
			required: true
		},

		real_name: { 
			type: String,
			required: true
		},

		email: { 
			type: String,
			required: true
		},

		slack_id: { 
			type: String,
			required: true
		},

		password: {
			type: String,
		},

		convo: {
			type: Object,

			actions: { 
				type: Array,
			},

			params: { 
				type: Object,
			},

			last_message: {
				type: String,
			},

			default: {
				actions: [],
				params: {new_user: true},
				last_message: "",
			},
		},

		permissions: { 
			type: Array,
			default: []
		},

		payslips: [
			{
				type: String, 
				ref: 'payslips'
			}
		],

		_create: {
			type: Object
		}
	});

	module.exports = UserSchema;
}())